package id.andimalik.customerapp.entities;

import id.andimalik.customerapp.dtos.CustomerFormDto;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.TableGenerator;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	@TableGenerator(name = "customerIdGen", table = "CUSTOMER_ID_GEN")
	@Setter
	private Long id;

	@Setter
	private String cifNumber;

	@Setter
	private String firstName;

	@Setter
	private String middleName;

	@Setter
	private String lastName;

	public Customer(CustomerFormDto dto) {
		this.cifNumber = dto.getCifNumber();
		this.firstName = dto.getFirstName();
		this.middleName = dto.getMiddleName();
		this.lastName = dto.getLastName();
	}

	public Long getId() {
		return this.id;
	}

	public String getCifNumber() {
		return this.cifNumber;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public String getLastName() {
		return this.lastName;
	}
}
