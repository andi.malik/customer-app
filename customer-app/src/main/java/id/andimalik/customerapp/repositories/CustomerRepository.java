package id.andimalik.customerapp.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import id.andimalik.customerapp.entities.Customer;
import jakarta.annotation.Nullable;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
	@Nullable
	public Customer findByCifNumber(String cif);

	@Query(value = "select c from Customer c where upper(concat(c.firstName, ' ', c.middleName, ' ', c.lastName)) like upper(concat('%', :keyword, '%')) order by concat(c.firstName, ' ', c.middleName, ' ', c.lastName)", countQuery = "select count(*) from Customer c where upper(concat(c.firstName, ' ', c.middleName, ' ', c.lastName)) like upper(concat('%', :keyword, '%'))")
	public Page<Customer> searchByName(@Param("keyword") String keyword, Pageable pageable);
}
