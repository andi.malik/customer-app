package id.andimalik.customerapp.dtos;

import id.andimalik.customerapp.entities.Customer;

public class CustomerDto {
	private Long id;
	private String cifNumber;
	private String firstName;
	private String middleName;
	private String lastName;

	public CustomerDto() {
		this.id = null;
		this.cifNumber = null;
		this.firstName = null;
		this.middleName = null;
		this.lastName = null;
	}

	public CustomerDto(Customer entity) {
		this.id = entity.getId();
		this.cifNumber = entity.getCifNumber();
		this.firstName = entity.getFirstName();
		this.middleName = entity.getMiddleName();
		this.lastName = entity.getLastName();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCifNumber() {
		return cifNumber;
	}

	public void setCifNumber(String cifNumber) {
		this.cifNumber = cifNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
