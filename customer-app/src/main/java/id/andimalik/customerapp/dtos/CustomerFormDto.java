package id.andimalik.customerapp.dtos;

public class CustomerFormDto {
	private String cifNumber;
	private String firstName;
	private String middleName;
	private String lastName;

	public CustomerFormDto() {
		this.cifNumber = null;
		this.firstName = null;
		this.middleName = null;
		this.lastName = null;
	}

	public String getCifNumber() {
		return this.cifNumber;
	}

	public void setCifNumber(String cifNumber) {
		this.cifNumber = cifNumber;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
