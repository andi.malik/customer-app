package id.andimalik.customerapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.andimalik.customerapp.dtos.CustomerDto;
import id.andimalik.customerapp.dtos.CustomerFormDto;
import id.andimalik.customerapp.services.CustomerService;

@RestController
public class CustomerController {
	private static final int PAGE_SIZE = 5;

	@Autowired
	private CustomerService service;

	@PostMapping(path = "/customers", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CustomerDto> registerCustomer(@RequestBody CustomerFormDto formDto) {
		CustomerDto dto = this.service.registerCustomer(formDto);
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	@GetMapping("/customers/{cifNumber}")
	public ResponseEntity<CustomerDto> getCustomer(@PathVariable String cifNumber) {
		CustomerDto dto = this.service.getCustomerByCifNumber(cifNumber);

		if (dto == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	@GetMapping("/customers/search/{keyword}/{pageIndex}")
	public ResponseEntity<Page<CustomerDto>> searchCustomer(@PathVariable String keyword, @PathVariable int pageIndex) {
		Pageable pageable = PageRequest.of(pageIndex, PAGE_SIZE);
		Page<CustomerDto> dtoPage = this.service.searchCustomerByName(keyword, pageable);

		return new ResponseEntity<>(dtoPage, HttpStatus.OK);
	}
}
