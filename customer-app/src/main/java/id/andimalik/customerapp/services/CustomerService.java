package id.andimalik.customerapp.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import id.andimalik.customerapp.dtos.CustomerDto;
import id.andimalik.customerapp.dtos.CustomerFormDto;

public interface CustomerService {
	public CustomerDto registerCustomer(CustomerFormDto formDto);

	public CustomerDto amendCustomer(CustomerFormDto formDto);

	public Page<CustomerDto> searchCustomerByName(String keyword, Pageable pageable);

	public CustomerDto getCustomerByCifNumber(String cifNumber);
}
