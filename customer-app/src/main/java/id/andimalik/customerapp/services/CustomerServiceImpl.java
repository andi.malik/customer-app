package id.andimalik.customerapp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import id.andimalik.customerapp.dtos.CustomerDto;
import id.andimalik.customerapp.dtos.CustomerFormDto;
import id.andimalik.customerapp.entities.Customer;
import id.andimalik.customerapp.repositories.CustomerRepository;
import lombok.Getter;
import lombok.Setter;

@Service
@Getter
@Setter
public class CustomerServiceImpl implements CustomerService {
	@Autowired
	private CustomerRepository repository;

	@Override
	public CustomerDto registerCustomer(CustomerFormDto formDto) {
		return this.saveCustomer(formDto);
	}

	@Override
	public CustomerDto amendCustomer(CustomerFormDto formDto) {
		return this.saveCustomer(formDto);
	}

	@Override
	public CustomerDto getCustomerByCifNumber(String cifNumber) {
		Customer entity = this.repository.findByCifNumber(cifNumber);

		if (entity == null) {
			return null;
		}

		return new CustomerDto(entity);
	}

	@Override
	public Page<CustomerDto> searchCustomerByName(String keyword, Pageable pageable) {
		Page<Customer> entityPage = this.repository.searchByName(keyword, pageable);
		return entityPage.map(CustomerDto::new);
	}

	private CustomerDto saveCustomer(CustomerFormDto formDto) {
		Customer entity = new Customer(formDto);
		this.repository.save(entity);

		return new CustomerDto(entity);
	}
}
